<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable=['title','description','status','created_by'];

    public function employees(){
        return $this->hasMany(Employee::class);
    }
}
