<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable=['name','dob','gender','mobile_number','email','address','department_id','photo','join_date','about','status','created_by','updated_by'];

    public function department(){
        return $this->belongsTo(Department::class);
    }
}