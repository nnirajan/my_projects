<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Department\CreateValidation;
use App\Http\Requests\Department\EditValidation;
use App\Model\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{
    //listing of departments
    public function index()
    {
        $data['title']="Listing Departments";
        $data['departments']=Department::all();
        return view('backend.department.index',compact('data'));
    }

    //display create form
    public function create()
    {
        $data['title']="Create Department";
        return view('backend/department/create',compact('data'));
    }

    //storing department data
    public function store(CreateValidation $request)
    {
        $request->request->add(['created_by' => Auth::user()->id]);
        Department::create($request->all());
        $request->session()->flash('success_message','Department Created Successfully');
        return redirect()->route('backend.department.index');
    }

    //view detail of a particular department
    public function show($id)
    {
        $data['department']=Department::find($id);
        $data['title']="View Detail of Department";
        return view('backend.department.show',compact('data'));
    }

    //show edit form
    public function edit($id)
    {
        $data['department']=Department::find($id);
        $data['title']="Edit Department Detail";
        return view('backend.department.edit',compact('data'));
    }

    //update the department data
    public function update(EditValidation $request, $id)
    {
        dd($request);
        if (!$row =  Department::find($id)){
            $request->session()->flash('error_message', 'Invalid Request !');
            return back();
        }
//        if($row->getOriginal('title') != $request->title) {
//            $this->validate($request, [
//                'title' => 'required|unique:departments.title',
//            ]);
//        }
        $request->request->add(['updated_by' => Auth::user()->id]);
        $row->update($request->all());
        $request->session()->flash('success_message', 'Department Information Updated Successfully');
        return redirect()->route('backend.department.index');
    }

    //delete department data
    public function destroy(Request $request,$id)
    {
        if (!$row =  Department::find($id)){
            $request->session()->flash('message', 'Invalid Request !');
            return back();
        }
        $row->delete();
        $request->session()->flash('success_message','Department Delete Successfully');
        return redirect()->route('backend.department.index');
    }
}