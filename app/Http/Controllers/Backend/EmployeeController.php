<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Employee\CreateValidation;
use App\Http\Requests\Employee\EditValidation;
use App\Model\Department;
use App\Model\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    //listing employees
    public function index()
    {
        $data['title']="Listing Employees";
        $data['employees']=Employee::all();
        return view('backend.employee.index',compact('data'));
    }

    //show create form
    public function create()
    {
        $data['title']="Create Employee";
        $data['department_id']=Department::where('status','1')->pluck('title','id');
        $data['department_id']->prepend('Select Department','');
        return view('backend.employee.create',compact('data'));
    }

    //storing of employee information
    public function store(CreateValidation $request)
    {
        if($request->file('employee_photo')){
            $photo = $request->file('employee_photo');
            $photo_name = rand(6785, 9814) . '_' . $photo->getClientOriginalName();
            $photo->move('images/employee', $photo_name);
            $request->request->add(['photo'=>$photo_name]);
        }
        $request->request->add(['created_by' => Auth::user()->id]);
        Employee::create($request->all());
        $request->session()->flash('success_message','Department Created Successfully');
        return redirect()->route('backend.employee.index');
    }

    //details of selected employee
    public function show($id)
    {
        $data['employee']=Employee::find($id);
        $data['title']="View Detail of Employee";
        return view('backend.employee.show',compact('data'));
    }

    //show edit form
    public function edit($id)
    {
        $data['employee']=Employee::find($id);
        $data['title']="Edit Employee Detail";
        $data['department_id']=Department::where('status','1')->pluck('title','id');
        $data['department_id']->prepend($data['employee']->department->title , $data['employee']->department->id );
        $data['department_id']->prepend('Select Department','');
        return view('backend.employee.edit',compact('data'));
    }

    //update employee information
    public function update(EditValidation $request,$id)
    {
//        dd($id);
        if (!$row =  Employee::find($id)){
            $request->session()->flash('error_message', 'Invalid Request !');
            return back();
        }

        if($row->getOriginal('email') != $request->email) {
            $this->validate($request, [
                'email' => 'required|unique:employees|email'
            ]);
        }

        //upload new photo and delete old photo
        if($request->file('employee_photo')){
            $photo = $request->file('employee_photo');
            $photo_name = rand(6785, 9814) . '_' . $photo->getClientOriginalName();
            $photo->move('images/employee', $photo_name);
            $request->request->add(['photo'=>$photo_name]);

            //remove existing photo
            if (file_exists('images/employee.' . $row->photo))
                unlink('images/employee.' . $row->photo);
        }

        $request->request->add(['updated_by' => Auth::user()->id]);
        $row->update($request->all());
        $request->session()->flash('success_message', 'Employee Information Updated Successfully');
        return redirect()->route('backend.employee.index');
    }

    //delete employee information
    public function destroy(Request $request,$id)
    {
        if (!$row =  Employee::find($id)){
            $request->session()->flash('message', 'Invalid Request !');
            return back();
        }
        //Remove old photo from folder
        if($row->photo && file_exists('images/employee/'.$row->photo)){
            unlink('images/employee/'.$row->photo);
        }
        $row->delete();
        $request->session()->flash('success_message','Employee Delete Successfully');
        return redirect()->route('backend.employee.index');
    }
}