<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <i class="fa fa-user"></i>
            </div>
            <div class="pull-left info">
                <p>
                    {{ Auth::user()->name }}</p>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li >
                <a href="{{ route('backend.dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            {{--Department--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-building"></i>
                    <span>Department Management</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('backend.department.create') }}"><i class="fa fa-circle-o"></i> Create</a></li>
                    <li><a href="{{ route('backend.department.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
                </ul>
            </li>

            {{--Employee--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Employee Management</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('backend.employee.create') }}"><i class="fa fa-circle-o"></i> Create</a></li>
                    <li><a href="{{ route('backend.employee.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
                </ul>
            </li>

        </ul>

    </section>
    <!-- /.sidebar -->
</aside>