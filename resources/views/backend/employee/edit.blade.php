@extends('backend.layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $data['title'] }}
                <small><a href="{{ route('backend.employee.index') }}" class="btn btn-info">List Employees</a>
                </small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">

                        @include('backend.includes.flash_message')

                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach

                        <!-- form start -->
                        {!! Form::model($data['employee'],[ 'route'=>['backend.employee.update','employee_id'  =>  $data['employee']->id ] ,'method'=>'PUT','files'=>'true']) !!}
                            @include('backend.employee.includes.main_form')
                            <div class="box-footer">
                                {!! Form::submit('Submit',['class'=> 'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (left) -->

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection