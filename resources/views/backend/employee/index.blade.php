@extends('backend.layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $data['title'] }}
                <small><a href="{{ route('backend.employee.create') }}" class="btn btn-info">Create Employee</a>
                </small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            @include('backend.includes.flash_message')

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <table id="department_table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>S.no</th>
                                        <th>Department</th>
                                        <th>Name</th>
                                        <th>Age</th>
                                        <th>Mobile Number</th>
                                        <th>Employeed For</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp
                                @foreach($data['employees'] as $employee)
                                    <tr>

                                        @php
                                            $now = date('Y-m-d');
                                            $dob = $employee->dob;
                                            $age = abs(strtotime($now) - strtotime($dob));
                                            $years = floor($age / (365*60*60*24));

                                            $join_date = abs(strtotime($now) - strtotime($employee->join_date));
                                            $join_years = floor( $join_date / (365*60*60*24) );
                                            $join_months = floor( ($join_date - $join_years * (365*60*60*24) ) / (30*60*60*24) );
                                            $join_days = floor( ($join_date - $join_years * (365*60*60*24) - $join_months*(30*60*60*24) ) / (60*60*24) )
                                        @endphp

                                        <td>{{ $i++ }}</td>
                                        <td>{{ $employee->department->title }}</td>
                                        <td>{{ $employee->name }}</td>
                                        <td>{{ $years }} years</td>
                                        <td>{{ $employee->mobile_number }}</td>
                                        <td>{{ $join_years }} years {{ $join_months }} months {{ $join_days }} days </td>
                                        <td>
                                            @if($employee->status==1)
                                                <span class="label label-success">Active</span>
                                            @else
                                                <span class="label label-warning">De-active</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('backend.employee.show', ['id' => $employee->id]) }}" class="btn btn-info">View</a>
                                            <a href="{{ route('backend.employee.edit', ['id' => $employee->id]) }}" class="btn btn-primary">Edit</a>
                                            {!! Form::open(['method'=>'POST','route'=>['backend.employee.destroy',$employee->id] ]) !!}
                                                {{ method_field('DELETE') }}
                                                {{ Form::submit('Delete', ['class'=>'btn btn-danger','onclick'=>"return confirm('Are you sure to delete this Employee?')" ] ) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>S.no</th>
                                    <th>Department</th>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Mobile Number</th>
                                    <th>Employeed For</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
    {{--<!-- DataTables -->--}}
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#department_table').DataTable()
        })
    </script>
@endsection


