@extends('backend.layouts.master')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $data['title'] }}
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">

                                            @php
                                                $now = date('Y-m-d');
                                                $dob = $data['employee']->dob;
                                                $age = abs(strtotime($now) - strtotime($dob));
                                                $years = floor($age / (365*60*60*24));

                                                $join_date = abs(strtotime($now) - strtotime($data['employee']->join_date));
                                                $join_years = floor( $join_date / (365*60*60*24) );
                                                $join_months = floor( ($join_date - $join_years * (365*60*60*24) ) / (30*60*60*24) );
                                                $join_days = floor( ($join_date - $join_years * (365*60*60*24) - $join_months*(30*60*60*24) ) / (60*60*24) )
                                            @endphp


                                            <tr>
                                                <th width="25%">Department</th>
                                                <td>{{$data['employee']->department->title}}</td>
                                            </tr>
                                            <tr>
                                                <th>Name</th>
                                                <td>{{$data['employee']->name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Date of Birth</th>
                                                <td>{{$data['employee']->dob}} ( {{ $years }} years )  </td>
                                            </tr>
                                            <tr>
                                                <th>Gender</th>
                                                <td>{{$data['employee']->name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Mobile Number</th>
                                                <td>{{$data['employee']->mobile_number}}</td>
                                            </tr>
                                            <tr>
                                                <th>Email Address</th>
                                                <td>{{$data['employee']->email}}</td>
                                            </tr>
                                            <tr>
                                                <th>Address</th>
                                                <td>{{$data['employee']->address}}</td>
                                            </tr>
                                            <tr>
                                                <th>Photo</th>
                                                <td><img src="{{ asset('images/employee/'.$data['employee']->photo) }}" height="100" width="100"></td>
                                            </tr>
                                            <tr>
                                                <th>Join Date</th>
                                                <td>{{$data['employee']->join_date}} ( {{ $join_years }} years {{ $join_months }} months {{ $join_days }} days  ) </td>
                                            </tr>
                                            <tr>
                                                <th>About</th>
                                                <td>{!! $data['employee']->about !!}</td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                @if($data['employee']->status == 1)
                                                    <td><span class="label label-success">Active</span></td>
                                                @else
                                                    <td><span class="label label-warning">De-active</span></td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="{{ route('backend.employee.edit', ['id' => $data['employee']->id]) }}" class="btn btn-primary">Edit</a>
                                                </td>
                                                <td>
                                                    {!! Form::open(['method'=>'POST','route'=>['backend.employee.destroy',$data['employee']->id] ]) !!}
                                                        {{ method_field('DELETE') }}
                                                        {{ Form::submit('Delete', ['class'=>'btn btn-danger','onclick'=>"return confirm('Are you sure to delete this Employee?')" ] ) }}
                                                    {{ Form::close() }}
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection