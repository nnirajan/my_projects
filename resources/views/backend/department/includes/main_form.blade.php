<div class="box-body">
    <div class="form-group">
        {{ Form::label('title', 'Department Title', ['class' => 'control-label']) }}
        {{ Form::text('title', null, ['class' => 'form-control','placeholder'=>'Enter Department Title']) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'title'])
    </div>

    <div class="form-group">
        {{ Form::label('description', 'Description', ['class' => 'control-label']) }}
        {{ Form::textarea('description', null, ['class' => 'form-control ckeditor']) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'description'])
    </div>

    <div class="form-group">
        {{ Form::label('status',null, ['class' => 'control-label']) }}
        <label class="radio-inline"> {!! Form::radio('status', '1', false) !!}Active </label>
        <label class="radio-inline"> {!! Form::radio('status', '0', true) !!}De-active </label>
    </div>
</div>
<!-- /.box-body -->