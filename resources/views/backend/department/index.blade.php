@extends('backend.layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $data['title'] }}
                <small><a href="{{ route('backend.department.create') }}" class="btn btn-info">Create Department</a>
                </small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            @include('backend.includes.flash_message')

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <table id="department_table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>S.no</th>
                                        <th>Department Title</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($i=1)
                                @foreach($data['departments'] as $department)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $department->title }}</td>
                                        <td>
                                            @if($department->status==1)
                                                <span class="label label-success">Active</span>
                                            @else
                                                <span class="label label-warning">De-active</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('backend.department.show', ['id' => $department->id]) }}" class="btn btn-info">View</a>
                                            <a href="{{ route('backend.department.edit', ['id' => $department->id]) }}" class="btn btn-primary">Edit</a>
                                            {!! Form::open(["method"=>"POST","route"=>["backend.department.destroy",$department->id] ]) !!}
                                                {{ method_field('DELETE') }}
                                                {{ Form::submit('Delete', ['class'=>'btn btn-danger','onclick'=>"return confirm('Are you sure to delete this Department?')" ] ) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S.no</th>
                                        <th>Department Title</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
    {{--<!-- DataTables -->--}}
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#department_table').DataTable()
        })
    </script>
@endsection


