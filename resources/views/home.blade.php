@extends('backend.layouts.master')

@section('content')    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
    </section>
    @include('backend.includes.flash_message')

</div>


<!-- /.content-wrapper -->
@endsection
